# CMake generated Testfile for 
# Source directory: /home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/tests
# Build directory: /home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(regressiontests/simple "perl" "/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/tests/regressiontests-2016.4/gmxtest.pl" "simple" "-crosscompile" "-noverbose" "-nosuffix")
set_tests_properties(regressiontests/simple PROPERTIES  ENVIRONMENT "PATH=/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/lib:/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games")
add_test(regressiontests/complex "perl" "/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/tests/regressiontests-2016.4/gmxtest.pl" "complex" "-crosscompile" "-noverbose" "-nosuffix")
set_tests_properties(regressiontests/complex PROPERTIES  ENVIRONMENT "PATH=/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/lib:/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games")
add_test(regressiontests/kernel "perl" "/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/tests/regressiontests-2016.4/gmxtest.pl" "kernel" "-crosscompile" "-noverbose" "-nosuffix")
set_tests_properties(regressiontests/kernel PROPERTIES  ENVIRONMENT "PATH=/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/lib:/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games")
add_test(regressiontests/freeenergy "perl" "/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/tests/regressiontests-2016.4/gmxtest.pl" "freeenergy" "-crosscompile" "-noverbose" "-nosuffix")
set_tests_properties(regressiontests/freeenergy PROPERTIES  ENVIRONMENT "PATH=/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/lib:/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games")
add_test(regressiontests/pdb2gmx "perl" "/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/tests/regressiontests-2016.4/gmxtest.pl" "pdb2gmx" "-crosscompile" "-noverbose" "-nosuffix")
set_tests_properties(regressiontests/pdb2gmx PROPERTIES  ENVIRONMENT "PATH=/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/lib:/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games")
add_test(regressiontests/rotation "perl" "/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/tests/regressiontests-2016.4/gmxtest.pl" "rotation" "-crosscompile" "-noverbose" "-nosuffix")
set_tests_properties(regressiontests/rotation PROPERTIES  ENVIRONMENT "PATH=/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/lib:/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games")
