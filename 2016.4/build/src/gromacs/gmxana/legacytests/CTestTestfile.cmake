# CMake generated Testfile for 
# Source directory: /home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/src/gromacs/gmxana/legacytests
# Build directory: /home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/src/gromacs/gmxana/legacytests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(LegacyToolsTests "/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/bin/legacy-tools-test" "--gtest_output=xml:/home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/Testing/Temporary/LegacyToolsTests.xml")
set_tests_properties(LegacyToolsTests PROPERTIES  LABELS "IntegrationTest")
