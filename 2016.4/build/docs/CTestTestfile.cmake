# CMake generated Testfile for 
# Source directory: /home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/docs
# Build directory: /home/fabio/gromacs/gromacs_files/gromacs_git/gromacs_efield/gromacs-2016.4/build/docs
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("doxygen")
subdirs("manual")
